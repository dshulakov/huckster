package kz.dan.huckster.repository;

import kz.dan.huckster.entity.Customer;
import kz.dan.huckster.entity.Product;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Query(value = "from Customer where (:name is null or name like lower(concat('%', :name,'%')))",
            countQuery = "select count(id) from Customer where (:name is null or lower(name) like lower(concat('%', :name,'%')))")
    PageImpl<Customer> findAll(@Param("name") String user, Pageable pageable);
}
