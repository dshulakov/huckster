package kz.dan.huckster.mapper.supplier;

import kz.dan.huckster.dto.supplier.SupplierCreateDto;
import kz.dan.huckster.entity.Supplier;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

@Component
public class SupplierCreateDtoMapper extends ModelMapper {
    public SupplierCreateDtoMapper() {
        super();
        this.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        createTypeMaps();
    }

    public Supplier toSupplier(SupplierCreateDto supplierCreateDto) {
        return this.map(supplierCreateDto, Supplier.class);
    }

    private void createTypeMaps() {
        TypeMap<SupplierCreateDto, Supplier> toSupplierTypeMap = this.createTypeMap(SupplierCreateDto.class, Supplier.class);
        toSupplierTypeMap.addMapping(SupplierCreateDto::getName, Supplier::setName);
        toSupplierTypeMap.addMapping(SupplierCreateDto::getName, Supplier::setName);
    }
}
