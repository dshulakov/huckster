package kz.dan.huckster.service.impl;

import kz.dan.huckster.dto.product.ProductCardDto;
import kz.dan.huckster.dto.product.ProductCreateDto;
import kz.dan.huckster.dto.product.ProductDto;
import kz.dan.huckster.entity.Product;
import kz.dan.huckster.mapper.product.ProductCreateDtoMapper;
import kz.dan.huckster.mapper.product.ProductMapper;
import kz.dan.huckster.service.ProductMapperService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductMapperServiceImpl implements ProductMapperService {
    private final ProductCreateDtoMapper productCreateDtoMapper;
    private final ProductMapper productMapper;
    @Override
    public Product toProduct(ProductCreateDto createDto) {
        return productCreateDtoMapper.toProduct(createDto);
    }

    @Override
    public ProductDto toProductDto(Product product) {
        return productMapper.toProductDto(product);
    }

    @Override
    public ProductCardDto toProductCardDto(Product product) {
        return productMapper.toProductCardDto(product);
    }
}
