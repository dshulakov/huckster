package kz.dan.huckster.service.impl;

import kz.dan.huckster.dto.size.SizeCreateDto;
import kz.dan.huckster.dto.size.SizeDto;
import kz.dan.huckster.dto.size.SizeListFilterDto;
import kz.dan.huckster.dto.size.SizeUpdateDto;
import kz.dan.huckster.entity.Size;
import kz.dan.huckster.exception.SizeNotFoundException;
import kz.dan.huckster.mapper.size.SizeMapper;
import kz.dan.huckster.repository.SizeRepository;
import kz.dan.huckster.service.SizeMapperService;
import kz.dan.huckster.service.SizeService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SizeServiceImpl implements SizeService {
    private final SizeRepository sizeRepository;
    private final SizeMapperService sizeMapperService;

    @Override
    public SizeDto create(SizeCreateDto sizeCreateDto) {
        Size size = sizeMapperService.toSize(sizeCreateDto);
        sizeRepository.save(size);
        return sizeMapperService.toSizeDto(size);
    }

    @Override
    public Page<SizeDto> get(SizeListFilterDto filter, Pageable pageable) {
        Page<Size> sizePage = sizeRepository.findAll(filter.getName(), pageable);
        return sizePage.map(sizeMapperService::toSizeDto);
    }

    @Override
    public SizeDto update(long id, SizeUpdateDto sizeUpdateDto) {
        Optional<Size> optionalSize = sizeRepository.findById(id);
        if (optionalSize.isEmpty()) {
            throw new RuntimeException("size not found");
        }
        Size size = optionalSize.get();
        size.setName(sizeUpdateDto.getName());
        sizeRepository.save(size);
        return sizeMapperService.toSizeDto(size);
    }

    @Override
    public void delete(long id) {
        Optional<Size> optionalProduct = sizeRepository.findById(id);
        if (optionalProduct.isEmpty()) {
            throw new RuntimeException("size not found");
        }
        Size size = optionalProduct.get();
        sizeRepository.delete(size);
    }

    @Override
    public Optional<Size> findById(long id) {
        return sizeRepository.findById(id);
    }

    @Override
    public Size getById(long id) {
        Optional<Size> optional = findById(id);
        if (optional.isEmpty()) {
            throw new SizeNotFoundException(id);
        }
        return optional.get();
    }

}
