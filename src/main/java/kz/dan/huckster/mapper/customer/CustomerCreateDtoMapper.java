package kz.dan.huckster.mapper.customer;

import kz.dan.huckster.dto.customer.CustomerCreateDto;
import kz.dan.huckster.entity.Customer;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

@Component
public class CustomerCreateDtoMapper extends ModelMapper {
    public CustomerCreateDtoMapper() {
        super();
        this.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        createTypeMaps();
    }

    public Customer toCustomer(CustomerCreateDto createDto) {
        return this.map(createDto, Customer.class);
    }
    private void createTypeMaps() {
        TypeMap<CustomerCreateDto, Customer> toCustomerTypeMap = this.createTypeMap(CustomerCreateDto.class, Customer.class);
        toCustomerTypeMap.addMapping(CustomerCreateDto::getName, Customer::setName);
    }

}
