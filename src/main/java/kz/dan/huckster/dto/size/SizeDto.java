package kz.dan.huckster.dto.size;

import lombok.Data;

@Data
public class SizeDto {
    private long id;
    private String name;
}
