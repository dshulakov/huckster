package kz.dan.huckster.dto.event;

import kz.dan.huckster.dto.product.ProductDto;
import lombok.Data;

@Data
public class EventSaleItem {
    private long id;
    private ProductDto product;
    private double unitPrice;
}
