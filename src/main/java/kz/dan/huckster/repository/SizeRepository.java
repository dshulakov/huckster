package kz.dan.huckster.repository;

import kz.dan.huckster.entity.Size;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SizeRepository extends JpaRepository<Size, Long> {
    @Query(value = "from Size where (:name is null or name like lower(concat('%', :name,'%')))",
            countQuery = "select count(id) from Size where (:name is null or lower(name) like lower(concat('%', :name,'%')))")
    PageImpl<Size> findAll(@Param("name") String user, Pageable pageable);
}
