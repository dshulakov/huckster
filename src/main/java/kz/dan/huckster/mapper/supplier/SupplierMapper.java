package kz.dan.huckster.mapper.supplier;

import kz.dan.huckster.dto.supplier.SupplierDto;
import kz.dan.huckster.entity.Supplier;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.TypeToken;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SupplierMapper extends ModelMapper {
    public SupplierMapper() {
        super();
        this.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        createTypeMaps();
    }

    public List<SupplierDto> toListSupplierDto(List<Supplier> suppliers) {
        return this.map(suppliers, new TypeToken<List<SupplierDto>>(){}.getType());
    }

    public SupplierDto toSupplierDto(Supplier supplier) {
        return this.map(supplier, SupplierDto.class);
    }

    private void createTypeMaps() {
        TypeMap<Supplier, SupplierDto> toSupplierDtoTypeMap = this.createTypeMap(Supplier.class, SupplierDto.class);
        toSupplierDtoTypeMap.addMapping(Supplier::getId, SupplierDto::setId);
        toSupplierDtoTypeMap.addMapping(Supplier::getName, SupplierDto::setName);
    }

}
