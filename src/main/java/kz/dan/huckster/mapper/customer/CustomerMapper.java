package kz.dan.huckster.mapper.customer;

import kz.dan.huckster.dto.customer.CustomerCreateDto;
import kz.dan.huckster.dto.customer.CustomerDto;
import kz.dan.huckster.entity.Customer;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.TypeToken;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CustomerMapper extends ModelMapper {
    public CustomerMapper() {
        super();
        this.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        createTypeMaps();
    }

    public CustomerDto toCustomerDto(Customer customer) {
        return this.map(customer, CustomerDto.class);
    }

    private void createTypeMaps() {
        TypeMap<Customer, CustomerDto> toCustomerDtoTypeMap = this.createTypeMap(Customer.class, CustomerDto.class);
        toCustomerDtoTypeMap.addMapping(Customer::getId, CustomerDto::setId);
        toCustomerDtoTypeMap.addMapping(Customer::getName, CustomerDto::setName);
    }

}
