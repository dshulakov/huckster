package kz.dan.huckster.service;

import kz.dan.huckster.dto.product.*;
import kz.dan.huckster.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface ProductService {
    ProductDto create(ProductCreateDto productCreateDto);

    Page<ProductDto> get(ProductListFilterDto filter, Pageable pageable);

    ProductDto update(long id, ProductUpdateDto productUpdateDto);

    void delete(long id);

    Optional<Product> findById(long id);
    Product getById(long id);
    ProductCardDto getProductDetails(Long id);
}

