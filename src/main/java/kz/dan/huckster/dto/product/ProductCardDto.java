package kz.dan.huckster.dto.product;

import kz.dan.huckster.dto.size.SizeDto;
import kz.dan.huckster.dto.supplier.SupplierDto;
import lombok.Data;

import java.util.List;

@Data
public class ProductCardDto {
    private long id;
    private String name;
    private SupplierDto supplier;
    private List<SizeDto> sizes;
}
