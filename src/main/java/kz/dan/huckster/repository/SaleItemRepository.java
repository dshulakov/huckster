package kz.dan.huckster.repository;

import kz.dan.huckster.entity.Event;
import kz.dan.huckster.entity.SaleItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SaleItemRepository extends JpaRepository<SaleItem, Long> {
    List<SaleItem> findAllByEvent(Event event);
}
