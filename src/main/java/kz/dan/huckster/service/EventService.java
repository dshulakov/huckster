package kz.dan.huckster.service;

import kz.dan.huckster.dto.event.*;
import kz.dan.huckster.entity.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface EventService {
    EventDto create(EventCreateDto eventCreateDto);

    Page<EventDto> get(EventListFilterDto filter, Pageable pageable);

    EventDto update(long id, EventUpdateDto eventUpdateDto);

    void delete(long id);

    Optional<Event> findById(long id);

    Event getById(long id) ;

}

