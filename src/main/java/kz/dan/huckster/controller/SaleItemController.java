package kz.dan.huckster.controller;

import kz.dan.huckster.dto.saleItem.SaleItemByEventDto;
import kz.dan.huckster.dto.saleItem.SaleItemCreateDto;
import kz.dan.huckster.dto.saleItem.SaleItemDto;
import kz.dan.huckster.service.SaleItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/saleitem")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class SaleItemController {
    private final SaleItemService saleItemService;

    @PostMapping
    public ResponseEntity<SaleItemDto> create(@Valid @RequestBody SaleItemCreateDto saleItemCreateDto) {
        SaleItemDto saleItemDto = saleItemService.create(saleItemCreateDto);
        return new ResponseEntity<>(saleItemDto, HttpStatus.CREATED);
    }

    @GetMapping("/by_event")
    public ResponseEntity<List<SaleItemByEventDto>> saleItems(@RequestParam(name = "event_id") long eventId) {
        List<SaleItemByEventDto> eventSaleItems = saleItemService.findByEvent(eventId);
        return new ResponseEntity<>(eventSaleItems, HttpStatus.OK);
    }
}
