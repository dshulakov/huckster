package kz.dan.huckster.dto.customer;

import lombok.Data;

@Data
public class CustomerDto {
    private long id;
    private String name;
}
