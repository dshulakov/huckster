package kz.dan.huckster.dto.customer;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Data
public class CustomerUpdateDto {
    @NotBlank
    @Size(min = 1, max = 255)
    private String name;
}
