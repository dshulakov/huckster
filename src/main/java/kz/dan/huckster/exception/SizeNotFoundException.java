package kz.dan.huckster.exception;

public class SizeNotFoundException extends RuntimeException{
    public SizeNotFoundException(long id) {
        super("size id = " + id + " not found ");
    }
}
