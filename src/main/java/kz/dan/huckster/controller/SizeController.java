package kz.dan.huckster.controller;

import kz.dan.huckster.dto.PageDto;
import kz.dan.huckster.dto.size.SizeCreateDto;
import kz.dan.huckster.dto.size.SizeDto;
import kz.dan.huckster.dto.size.SizeListFilterDto;
import kz.dan.huckster.dto.size.SizeUpdateDto;
import kz.dan.huckster.service.SizeService;
import kz.dan.huckster.utils.PageUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/size")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class SizeController {
    private final SizeService sizeService;

    @PostMapping
    public ResponseEntity<SizeDto> create(@Valid @RequestBody SizeCreateDto sizeCreateDto) {
        SizeDto sizeDto = sizeService.create(sizeCreateDto);
        return new ResponseEntity<>(sizeDto, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Page<SizeDto>> get(@RequestParam(required = false) String name,
                                                PageDto pageDto) {
        SizeListFilterDto filter = SizeListFilterDto.builder()
                .name(name)
                .build();
        Page<SizeDto> all = sizeService.get(filter, PageUtils.createPageRequest(pageDto));
        return new ResponseEntity<>(all, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<SizeDto> update(@PathVariable(name = "id") long id, @Valid @RequestBody SizeUpdateDto sizeUpdateDto) {
        SizeDto sizeDto = sizeService.update(id, sizeUpdateDto);
        return new ResponseEntity<>(sizeDto, HttpStatus.OK);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public void delete(@RequestParam(name = "id") long id) {
        sizeService.delete(id);
    }
}
