package kz.dan.huckster.service;

import kz.dan.huckster.dto.product.ProductCardDto;
import kz.dan.huckster.dto.product.ProductCreateDto;
import kz.dan.huckster.dto.product.ProductDto;
import kz.dan.huckster.entity.Product;

public interface ProductMapperService {
    Product toProduct(ProductCreateDto createDto);
    ProductDto toProductDto(Product product);
    ProductCardDto toProductCardDto(Product product);
}
