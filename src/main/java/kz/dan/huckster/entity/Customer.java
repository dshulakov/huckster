package kz.dan.huckster.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "customer")
@Data
public class Customer extends AbstractEntity {
    private String name;
}
