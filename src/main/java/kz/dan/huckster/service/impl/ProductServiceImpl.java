package kz.dan.huckster.service.impl;

import kz.dan.huckster.dto.product.*;
import kz.dan.huckster.entity.Product;
import kz.dan.huckster.entity.Supplier;
import kz.dan.huckster.exception.ProductNotFoundException;
import kz.dan.huckster.mapper.product.ProductMapper;
import kz.dan.huckster.repository.ProductRepository;
import kz.dan.huckster.service.ProductMapperService;
import kz.dan.huckster.service.ProductService;
import kz.dan.huckster.service.SupplierService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final ProductMapperService productMapperService;
    private final SupplierService supplierService;

    @Override
    public ProductDto create(ProductCreateDto productCreateDto) {
        Product product = productMapperService.toProduct(productCreateDto);
        productRepository.save(product);
        return productMapperService.toProductDto(product);
    }

    @Override
    public Page<ProductDto> get(ProductListFilterDto filter, Pageable pageable) {
        Supplier supplier = null;
        if (filter.getSupplierId() != null) {
            supplier = supplierService.getById(filter.getSupplierId());
        }
        Page<Product> productPage = productRepository.findAll(filter.getName(), supplier, pageable);
        return productPage.map(productMapperService::toProductDto);
    }

    @Override
    public ProductDto update(long id, ProductUpdateDto productUpdateDto) {
        Product product = getById(id);
        Supplier supplier = supplierService.getById(productUpdateDto.getSupplierId());
        product.setName(productUpdateDto.getName());
        product.setSupplier(supplier);
        productRepository.save(product);
        return productMapperService.toProductDto(product);
    }

    @Override
    public void delete(long id) {
        Product product = getById(id);
        productRepository.delete(product);
    }

    @Override
    public Optional<Product> findById(long id) {
        return productRepository.findById(id);
    }

    @Override
    public Product getById(long id) {
        Optional<Product> optionalProduct = findById(id);
        if (optionalProduct.isEmpty()) {
            throw new ProductNotFoundException(id);
        }
        return optionalProduct.get();
    }

    @Override
    public ProductCardDto getProductDetails(Long id) {
        Product product = getById(id);
        return productMapperService.toProductCardDto(product);
    }

}
