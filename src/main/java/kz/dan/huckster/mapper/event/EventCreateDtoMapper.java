package kz.dan.huckster.mapper.event;

import kz.dan.huckster.dto.event.EventCreateDto;
import kz.dan.huckster.entity.Event;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

@Component
public class EventCreateDtoMapper extends ModelMapper {
    public EventCreateDtoMapper() {
        super();
        this.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        createTypeMaps();
    }

    public Event toEvent(EventCreateDto eventCreateDto) {
        return this.map(eventCreateDto, Event.class);
    }

    private void createTypeMaps() {
        TypeMap<EventCreateDto, Event> toEventTypeMap = this.createTypeMap(EventCreateDto.class, Event.class);
        toEventTypeMap.addMapping(EventCreateDto::getName, Event::setName);
        toEventTypeMap.addMapping(EventCreateDto::getCurrency, Event::setCurrency);
        toEventTypeMap.addMapping(EventCreateDto::getRate, Event::setRate);
    }

}
