package kz.dan.huckster.service;

import kz.dan.huckster.dto.size.SizeCreateDto;
import kz.dan.huckster.dto.size.SizeDto;
import kz.dan.huckster.dto.size.SizeListFilterDto;
import kz.dan.huckster.dto.size.SizeUpdateDto;
import kz.dan.huckster.entity.Size;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface SizeService {
    SizeDto create(SizeCreateDto sizeCreateDto);

    Page<SizeDto> get(SizeListFilterDto filter, Pageable pageable);

    SizeDto update(long id, SizeUpdateDto sizeUpdateDto);

    void delete(long id);
    Optional<Size> findById(long id);

    Size getById(long id) ;

}

