package kz.dan.huckster.mapper.product.converter;

import kz.dan.huckster.entity.Product;
import kz.dan.huckster.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProductIdToEntityConverter implements Converter<Long, Product> {
    private final ProductService productService;
    @Override
    public Product convert(MappingContext<Long, Product> mappingContext) {
        return productService.getById(mappingContext.getSource());
    }
}
