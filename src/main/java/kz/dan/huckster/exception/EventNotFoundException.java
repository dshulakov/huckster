package kz.dan.huckster.exception;

public class EventNotFoundException extends RuntimeException{
    public EventNotFoundException(long id) {
        super("event id = " + id + " not found ");
    }
}
