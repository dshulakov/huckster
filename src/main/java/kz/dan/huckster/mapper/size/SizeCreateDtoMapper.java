package kz.dan.huckster.mapper.size;

import kz.dan.huckster.dto.size.SizeCreateDto;
import kz.dan.huckster.entity.Size;
import kz.dan.huckster.mapper.product.converter.ProductIdToEntityConverter;
import kz.dan.huckster.service.ProductService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

@Component
public class SizeCreateDtoMapper extends ModelMapper {
    public SizeCreateDtoMapper(ProductIdToEntityConverter productIdToEntityConverter) {
        super();
        this.addConverter(productIdToEntityConverter);
        this.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        createTypeMaps();
    }

    public Size toSize(SizeCreateDto sizeCreateDto) {
        return this.map(sizeCreateDto, Size.class);
    }

    private void createTypeMaps() {
        TypeMap<SizeCreateDto, Size> typeMap = this.createTypeMap(SizeCreateDto.class, Size.class);
        typeMap.addMapping(SizeCreateDto::getName, Size::setName);
        typeMap.addMapping(SizeCreateDto::getProductId, Size::setProduct);
    }
}
