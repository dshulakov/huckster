package kz.dan.huckster.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sale_item")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SaleItem extends AbstractEntity{
    @ManyToOne
    @JoinColumn(name="event_id", nullable=false)
    private Event event;

    @ManyToOne
    @JoinColumn(name="product_id", nullable=false)
    private Product product;

    private double price;
}
