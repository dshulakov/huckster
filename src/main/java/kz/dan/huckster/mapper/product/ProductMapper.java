package kz.dan.huckster.mapper.product;

import kz.dan.huckster.dto.product.ProductCardDto;
import kz.dan.huckster.dto.product.ProductDto;
import kz.dan.huckster.entity.Product;
import kz.dan.huckster.mapper.supplier.converter.SupplierToSupplierDtoConverter;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper extends ModelMapper {

    public ProductMapper(SupplierToSupplierDtoConverter supplierToSupplierDtoConverter) {
        super();
        this.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        this.addConverter(supplierToSupplierDtoConverter);
        createTypeMaps();
    }

    public ProductDto toProductDto(Product product) {
        return this.map(product, ProductDto.class);
    }

    public ProductCardDto toProductCardDto(Product product) {
        return this.map(product, ProductCardDto.class);
    }

    private void createTypeMaps() {
        TypeMap<Product, ProductDto> toProductDtoTypeMap = this.createTypeMap(Product.class, ProductDto.class);
        toProductDtoTypeMap.addMapping(Product::getId, ProductDto::setId);
        toProductDtoTypeMap.addMapping(Product::getName, ProductDto::setName);
        toProductDtoTypeMap.addMapping(Product::getSupplier, ProductDto::setSupplier);

        TypeMap<Product, ProductCardDto> toProductCardDtoTypeMap = this.createTypeMap(Product.class, ProductCardDto.class);
        toProductCardDtoTypeMap.addMapping(Product::getId, ProductCardDto::setId);
        toProductCardDtoTypeMap.addMapping(Product::getName, ProductCardDto::setName);
        toProductCardDtoTypeMap.addMapping(Product::getSupplier, ProductCardDto::setSupplier);
        toProductCardDtoTypeMap.addMapping(Product::getSizes, ProductCardDto::setSizes);
    }

}
