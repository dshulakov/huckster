package kz.dan.huckster.service;

import kz.dan.huckster.dto.saleItem.SaleItemByEventDto;
import kz.dan.huckster.dto.saleItem.SaleItemCreateDto;
import kz.dan.huckster.dto.saleItem.SaleItemDto;

import java.util.List;

public interface SaleItemService {
    SaleItemDto create(SaleItemCreateDto saleItemCreateDto);

    List<SaleItemByEventDto> findByEvent(long eventId);
}

