package kz.dan.huckster.dto.supplier;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SupplierListFilterDto {
    private String name;
}
