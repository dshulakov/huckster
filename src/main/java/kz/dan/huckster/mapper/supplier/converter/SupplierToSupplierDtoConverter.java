package kz.dan.huckster.mapper.supplier.converter;

import kz.dan.huckster.dto.supplier.SupplierDto;
import kz.dan.huckster.entity.Supplier;
import kz.dan.huckster.service.SupplierMapperService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SupplierToSupplierDtoConverter implements Converter<Supplier, SupplierDto> {
    private final SupplierMapperService supplierMapperService;
    @Override
    public SupplierDto convert(MappingContext<Supplier, SupplierDto> mappingContext) {
        return supplierMapperService.toSupplierDto(mappingContext.getSource());
    }
}
