package kz.dan.huckster.controller;

import kz.dan.huckster.dto.PageDto;
import kz.dan.huckster.dto.event.EventCreateDto;
import kz.dan.huckster.dto.event.EventDto;
import kz.dan.huckster.dto.event.EventListFilterDto;
import kz.dan.huckster.dto.event.EventUpdateDto;
import kz.dan.huckster.service.EventService;
import kz.dan.huckster.utils.PageUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/event")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class EventController {
    private final EventService eventService;

    @PostMapping
    public ResponseEntity<EventDto> create(@Valid @RequestBody EventCreateDto eventCreateDto) {
        EventDto eventDto = eventService.create(eventCreateDto);
        return new ResponseEntity<>(eventDto, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Page<EventDto>> get(@RequestParam(required = false) String name,
                                              PageDto pageDto) {
        EventListFilterDto filter = EventListFilterDto.builder()
                .name(name)
                .build();
        Page<EventDto> all = eventService.get(filter, PageUtils.createPageRequest(pageDto));
        return new ResponseEntity<>(all, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<EventDto> update(@PathVariable(name = "id") long id, @Valid @RequestBody EventUpdateDto eventUpdateDto) {
        EventDto eventDto = eventService.update(id, eventUpdateDto);
        return new ResponseEntity<>(eventDto, HttpStatus.OK);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public void delete(@RequestParam(name = "id") long id) {
        eventService.delete(id);
    }

}
