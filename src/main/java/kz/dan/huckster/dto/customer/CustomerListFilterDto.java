package kz.dan.huckster.dto.customer;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerListFilterDto {
    private String name;
}
