package kz.dan.huckster.dto.event;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EventListFilterDto {
    private String name;
}
