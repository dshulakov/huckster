package kz.dan.huckster.mapper.size;

import kz.dan.huckster.dto.size.SizeDto;
import kz.dan.huckster.entity.Size;
import kz.dan.huckster.mapper.product.converter.ProductToProductDtoConverter;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.TypeToken;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SizeMapper extends ModelMapper {
    public SizeMapper() {
        super();
        this.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        createTypeMaps();
    }

    public SizeDto toSizeDto(Size size) {
        return this.map(size, SizeDto.class);
    }

    public List<SizeDto> toListSizeDto(List<Size> sizes) {
        return this.map(sizes, new TypeToken<List<SizeDto>>(){}.getType());
    }
    private void createTypeMaps() {
        TypeMap<Size, SizeDto> typeMap = this.createTypeMap(Size.class, SizeDto.class);
        typeMap.addMapping(Size::getId, SizeDto::setId);
        typeMap.addMapping(Size::getName, SizeDto::setName);
    }

}
