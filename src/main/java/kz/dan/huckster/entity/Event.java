package kz.dan.huckster.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "event")
@Data
public class Event extends AbstractEntity {
    @Column(name = "name", nullable = false, length = 150)
    @NotBlank
    private String name;
    @Column(name = "currency", nullable = false, length = 20)
    @NotBlank
    private String currency;
    private double rate;

}
