package kz.dan.huckster.service;

import kz.dan.huckster.dto.supplier.SupplierCreateDto;
import kz.dan.huckster.dto.supplier.SupplierDto;
import kz.dan.huckster.entity.Supplier;

import java.util.List;

public interface SupplierMapperService {
    Supplier toSupplier(SupplierCreateDto createDto);
    SupplierDto toSupplierDto(Supplier supplier);
    List<SupplierDto> toListSupplierDto(List<Supplier> suppliers);
}
