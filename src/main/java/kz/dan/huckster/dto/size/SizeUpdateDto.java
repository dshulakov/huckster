package kz.dan.huckster.dto.size;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class SizeUpdateDto {
    @NotBlank
    @Size(min = 1, max = 255)
    private String name;
}
