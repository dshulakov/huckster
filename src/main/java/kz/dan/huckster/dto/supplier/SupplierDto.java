package kz.dan.huckster.dto.supplier;

import lombok.Data;

@Data
public class SupplierDto {
    private long id;
    private String name;
}
