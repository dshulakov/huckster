package kz.dan.huckster.service.impl;

import kz.dan.huckster.dto.supplier.SupplierCreateDto;
import kz.dan.huckster.dto.supplier.SupplierDto;
import kz.dan.huckster.entity.Supplier;
import kz.dan.huckster.mapper.supplier.SupplierCreateDtoMapper;
import kz.dan.huckster.mapper.supplier.SupplierMapper;
import kz.dan.huckster.service.SupplierMapperService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SupplierMapperServiceImpl implements SupplierMapperService {
    private final SupplierCreateDtoMapper supplierCreateDtoMapper;
    private final SupplierMapper supplierMapper;
    @Override
    public Supplier toSupplier(SupplierCreateDto createDto) {
        return supplierCreateDtoMapper.toSupplier(createDto);
    }

    @Override
    public SupplierDto toSupplierDto(Supplier supplier) {
        return supplierMapper.toSupplierDto(supplier);
    }

    @Override
    public List<SupplierDto> toListSupplierDto(List<Supplier> suppliers) {
        return supplierMapper.toListSupplierDto(suppliers);
    }
}
