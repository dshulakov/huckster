package kz.dan.huckster.dto.size;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class SizeCreateDto {
    @NotBlank
    @Size(min = 1, max = 255)
    private String name;
    @JsonProperty("product_id")
    private Long productId;
}
