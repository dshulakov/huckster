package kz.dan.huckster.mapper.product.converter;

import kz.dan.huckster.dto.product.ProductDto;
import kz.dan.huckster.entity.Product;
import kz.dan.huckster.mapper.product.ProductMapper;
import kz.dan.huckster.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProductToProductDtoConverter implements Converter<Product, ProductDto> {
    private final ProductMapper productMapper;
    @Override
    public ProductDto convert(MappingContext<Product, ProductDto> mappingContext) {
        return productMapper.toProductDto(mappingContext.getSource());
    }
}
