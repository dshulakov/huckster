package kz.dan.huckster.dto.event;

import lombok.Data;

@Data
public class EventDto {
    private long id;
    private String name;
    private String currency;
    private double rate;
}
