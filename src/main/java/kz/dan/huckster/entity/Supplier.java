package kz.dan.huckster.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "supplier")
@Data
public class Supplier extends AbstractEntity {
    private String name;
}
