package kz.dan.huckster.controller;

import kz.dan.huckster.dto.PageDto;
import kz.dan.huckster.dto.product.*;
import kz.dan.huckster.service.ProductService;
import kz.dan.huckster.utils.PageUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class ProductController {
    private final ProductService productService;

    @PostMapping
    public ResponseEntity<ProductDto> create(@Valid @RequestBody ProductCreateDto productCreateDto) {
        ProductDto productDto = productService.create(productCreateDto);
        return new ResponseEntity<>(productDto, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Page<ProductDto>> get(@RequestParam(required = false) String name,
                                                @RequestParam(required = false, name = "supplier_id") Long supplierId,
                                                PageDto pageDto) {
        ProductListFilterDto filter = ProductListFilterDto.builder()
                .name(name)
                .supplierId(supplierId)
                .build();
        Page<ProductDto> all = productService.get(filter, PageUtils.createPageRequest(pageDto));
        return new ResponseEntity<>(all, HttpStatus.OK);
    }

    @GetMapping("/card")
    public ResponseEntity<ProductCardDto> getCard(@RequestParam(name = "id") Long id) {
        ProductCardDto productCardDto = productService.getProductDetails(id);
        return new ResponseEntity<>(productCardDto, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductDto> update(@PathVariable(name = "id") long id, @Valid @RequestBody ProductUpdateDto productUpdateDto) {
        ProductDto productDto = productService.update(id, productUpdateDto);
        return new ResponseEntity<>(productDto, HttpStatus.OK);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public void delete(@RequestParam(name = "id") long id) {
        productService.delete(id);
    }
}
