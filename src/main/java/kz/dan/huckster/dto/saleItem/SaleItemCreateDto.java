package kz.dan.huckster.dto.saleItem;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SaleItemCreateDto {

    @JsonProperty("event_id")
    private Long eventId;
    @JsonProperty("product_id")
    private Long productId;
    @JsonProperty("price")
    private Double price;
}
