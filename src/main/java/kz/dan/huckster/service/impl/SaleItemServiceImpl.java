package kz.dan.huckster.service.impl;

import kz.dan.huckster.dto.saleItem.SaleItemByEventDto;
import kz.dan.huckster.dto.saleItem.SaleItemCreateDto;
import kz.dan.huckster.dto.saleItem.SaleItemDto;
import kz.dan.huckster.entity.Event;
import kz.dan.huckster.entity.Product;
import kz.dan.huckster.entity.SaleItem;
import kz.dan.huckster.exception.EventNotFoundException;
import kz.dan.huckster.exception.ProductNotFoundException;
import kz.dan.huckster.mapper.SaleItemMapper;
import kz.dan.huckster.repository.SaleItemRepository;
import kz.dan.huckster.service.EventService;
import kz.dan.huckster.service.ProductService;
import kz.dan.huckster.service.SaleItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SaleItemServiceImpl implements SaleItemService {
    private final SaleItemRepository saleItemRepository;
    private final SaleItemMapper saleItemMapper;
    private final EventService eventService;
    private final ProductService productService;

    @Override
    public SaleItemDto create(SaleItemCreateDto saleItemCreateDto) {
        Event event = eventService.getById(saleItemCreateDto.getEventId());
        Product product = productService.getById(saleItemCreateDto.getProductId());
        SaleItem saleItem = SaleItem.builder()
                .event(event)
                .product(product)
                .price(saleItemCreateDto.getPrice())
                .build();
        saleItemRepository.save(saleItem);

        return saleItemMapper.mapSaleItemToSaleItemDto(saleItem);
    }

    @Override
    public List<SaleItemByEventDto> findByEvent(long eventId) {
        Event event = eventService.getById(eventId);
        List<SaleItem> saleItems = saleItemRepository.findAllByEvent(event);
        return saleItemMapper.mapListSaleItemToListSaleItemByEventDto(saleItems);
    }

}
