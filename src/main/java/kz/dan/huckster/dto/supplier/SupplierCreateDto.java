package kz.dan.huckster.dto.supplier;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class SupplierCreateDto {
    @NotBlank
    @Size(min = 1, max = 255)
    private String name;
}
