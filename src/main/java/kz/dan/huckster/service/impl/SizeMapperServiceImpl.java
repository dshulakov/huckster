package kz.dan.huckster.service.impl;

import kz.dan.huckster.dto.size.SizeCreateDto;
import kz.dan.huckster.dto.size.SizeDto;
import kz.dan.huckster.entity.Size;
import kz.dan.huckster.mapper.size.SizeCreateDtoMapper;
import kz.dan.huckster.mapper.size.SizeMapper;
import kz.dan.huckster.service.SizeMapperService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SizeMapperServiceImpl implements SizeMapperService {
    private final SizeCreateDtoMapper sizeCreateDtoMapper;
    private final SizeMapper sizeMapper;
    @Override
    public Size toSize(SizeCreateDto createDto) {
        return sizeCreateDtoMapper.toSize(createDto);
    }

    @Override
    public SizeDto toSizeDto(Size size) {
        return sizeMapper.toSizeDto(size);
    }
}
