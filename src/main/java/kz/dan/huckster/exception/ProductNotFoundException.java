package kz.dan.huckster.exception;

public class ProductNotFoundException extends RuntimeException{
    public ProductNotFoundException(long id) {
        super("product id = " + id + " not found ");
    }
}
