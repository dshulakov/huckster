package kz.dan.huckster.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "size")
@Data
public class Size extends AbstractEntity {
    private String name;
    @ManyToOne
    @JoinColumn(name="product_id", nullable=false)
    private Product product;
}
