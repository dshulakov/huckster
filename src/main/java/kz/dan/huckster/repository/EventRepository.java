package kz.dan.huckster.repository;

import kz.dan.huckster.entity.Event;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends PagingAndSortingRepository<Event, Long> {
    @Query(value = "from Event where (:name is null or name like lower(concat('%', :name,'%')))",
            countQuery = "select count(id) from Event where (:name is null or lower(name) like lower(concat('%', :name,'%')))")
    PageImpl<Event> findAll(@Param("name") String user, Pageable pageable);
}
