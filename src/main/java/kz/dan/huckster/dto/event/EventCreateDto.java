package kz.dan.huckster.dto.event;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Data
public class EventCreateDto {
    @NotBlank
    @Size(min = 1, max = 150)
    private String name;

    @NotBlank
    @Size(min = 1, max = 20)
    private String currency;

    @PositiveOrZero
    private double rate;
}
