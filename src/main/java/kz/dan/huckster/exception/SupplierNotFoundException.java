package kz.dan.huckster.exception;

public class SupplierNotFoundException extends RuntimeException{
    public SupplierNotFoundException(long id) {
        super("supplier id = " + id + " not found ");
    }
}
