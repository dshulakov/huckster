package kz.dan.huckster.exception;

public class CustomerNotFoundException extends RuntimeException{
    public CustomerNotFoundException(long id) {
        super("customer id = " + id + " not found ");
    }
}
