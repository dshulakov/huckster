package kz.dan.huckster.utils;

import kz.dan.huckster.dto.PageDto;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

public class PageUtils {
    public static PageRequest createPageRequest(PageDto pageDto) {
        return PageRequest.of(
                pageDto.getPageNumber(),
                pageDto.getPageSize(),
                Sort.by(pageDto.getDirection(), pageDto.getSortBy()));
    }
}
