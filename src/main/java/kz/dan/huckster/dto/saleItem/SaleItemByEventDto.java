package kz.dan.huckster.dto.saleItem;

import kz.dan.huckster.dto.product.ProductDto;
import lombok.Data;

@Data
public class SaleItemByEventDto {
    private long id;
    private ProductDto product;
    private double price;
}
