package kz.dan.huckster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class HucksterApplication {

	public static void main(String[] args) {
		SpringApplication.run(HucksterApplication.class, args);
	}

}
