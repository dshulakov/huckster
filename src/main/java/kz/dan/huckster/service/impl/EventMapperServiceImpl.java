package kz.dan.huckster.service.impl;

import kz.dan.huckster.dto.event.EventCreateDto;
import kz.dan.huckster.dto.event.EventDto;
import kz.dan.huckster.entity.Event;
import kz.dan.huckster.mapper.event.EventCreateDtoMapper;
import kz.dan.huckster.mapper.event.EventMapper;
import kz.dan.huckster.service.EventMapperService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EventMapperServiceImpl implements EventMapperService {
    private final EventCreateDtoMapper eventCreateDtoMapper;
    private final EventMapper eventMapper;
    @Override
    public Event toEvent(EventCreateDto createDto) {
        return eventCreateDtoMapper.toEvent(createDto);
    }

    @Override
    public EventDto toEventDto(Event event) {
        return eventMapper.toEventDto(event);
    }
}
