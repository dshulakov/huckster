package kz.dan.huckster.service;

import kz.dan.huckster.dto.customer.CustomerCreateDto;
import kz.dan.huckster.dto.customer.CustomerDto;
import kz.dan.huckster.entity.Customer;

public interface CustomerMapperService {
    Customer toCustomer(CustomerCreateDto createDto);
    CustomerDto toCustomerDto(Customer customer);
}
