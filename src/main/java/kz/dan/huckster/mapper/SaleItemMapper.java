package kz.dan.huckster.mapper;

import kz.dan.huckster.dto.saleItem.SaleItemByEventDto;
import kz.dan.huckster.dto.saleItem.SaleItemDto;
import kz.dan.huckster.entity.SaleItem;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.TypeToken;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SaleItemMapper extends ModelMapper {
    public SaleItemMapper() {
        super();
        this.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        saleItemToSaleItemDtoTypeMap();
        saleItemToSaleItemByEventDtoTypeMap();
    }

    public SaleItemDto mapSaleItemToSaleItemDto(SaleItem saleItem) {
        return this.map(saleItem, SaleItemDto.class);
    }

    public List<SaleItemDto> mapListSaleItemToListSaleItemDto(List<SaleItem> saleItems) {
        return this.map(saleItems, new TypeToken<List<SaleItemDto>>(){}.getType());
    }

    private void saleItemToSaleItemDtoTypeMap() {
        TypeMap<SaleItem, SaleItemDto> typeMap = this.createTypeMap(SaleItem.class, SaleItemDto.class);
        typeMap.addMapping(SaleItem::getId, SaleItemDto::setId);
        typeMap.addMapping(SaleItem::getEvent, SaleItemDto::setEvent);
        typeMap.addMapping(SaleItem::getProduct, SaleItemDto::setProduct);
        typeMap.addMapping(SaleItem::getPrice, SaleItemDto::setPrice);
    }

    public SaleItemByEventDto mapSaleItemToSaleItemByEventDto(SaleItem saleItem) {
        return this.map(saleItem, SaleItemByEventDto.class);
    }

    public List<SaleItemByEventDto> mapListSaleItemToListSaleItemByEventDto(List<SaleItem> saleItems) {
        return this.map(saleItems, new TypeToken<List<SaleItemByEventDto>>(){}.getType());
    }

    private void saleItemToSaleItemByEventDtoTypeMap() {
        TypeMap<SaleItem, SaleItemByEventDto> typeMap = this.createTypeMap(SaleItem.class, SaleItemByEventDto.class);
        typeMap.addMapping(SaleItem::getId, SaleItemByEventDto::setId);
        typeMap.addMapping(SaleItem::getProduct, SaleItemByEventDto::setProduct);
        typeMap.addMapping(SaleItem::getPrice, SaleItemByEventDto::setPrice);
    }

}
