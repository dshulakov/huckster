package kz.dan.huckster.service.impl;

import kz.dan.huckster.dto.customer.CustomerCreateDto;
import kz.dan.huckster.dto.customer.CustomerDto;
import kz.dan.huckster.entity.Customer;
import kz.dan.huckster.mapper.customer.CustomerCreateDtoMapper;
import kz.dan.huckster.mapper.customer.CustomerMapper;
import kz.dan.huckster.service.CustomerMapperService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerMapperServiceImpl implements CustomerMapperService {
    private final CustomerCreateDtoMapper customerCreateDtoMapper;
    private final CustomerMapper customerMapper;
    @Override
    public Customer toCustomer(CustomerCreateDto createDto) {
        return customerCreateDtoMapper.toCustomer(createDto);
    }

    @Override
    public CustomerDto toCustomerDto(Customer customer) {
        return customerMapper.toCustomerDto(customer);
    }
}
