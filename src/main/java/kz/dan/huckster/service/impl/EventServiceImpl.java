package kz.dan.huckster.service.impl;

import kz.dan.huckster.dto.event.EventCreateDto;
import kz.dan.huckster.dto.event.EventDto;
import kz.dan.huckster.dto.event.EventListFilterDto;
import kz.dan.huckster.dto.event.EventUpdateDto;
import kz.dan.huckster.entity.Event;
import kz.dan.huckster.exception.EventNotFoundException;
import kz.dan.huckster.mapper.event.EventMapper;
import kz.dan.huckster.repository.EventRepository;
import kz.dan.huckster.service.EventMapperService;
import kz.dan.huckster.service.EventService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EventServiceImpl implements EventService {
    private final EventRepository eventRepository;
    private final EventMapperService eventMapperService;

    @Override
    public EventDto create(EventCreateDto eventCreateDto) {
        Event event = eventMapperService.toEvent(eventCreateDto);
        eventRepository.save(event);
        return eventMapperService.toEventDto(event);
    }

    @Override
    public Page<EventDto> get(EventListFilterDto filter, Pageable pageable) {
        Page<Event> eventPage = eventRepository.findAll(filter.getName(), pageable);
        return eventPage.map(eventMapperService::toEventDto);
    }

    @Override
    public EventDto update(long id, EventUpdateDto eventUpdateDto) {
        Event event = getById(id);
        event.setName(eventUpdateDto.getName());
        event.setCurrency(eventUpdateDto.getCurrency());
        event.setRate(eventUpdateDto.getRate());
        eventRepository.save(event);
        return eventMapperService.toEventDto(event);
    }

    @Override
    public void delete(long id) {
        Event event = getById(id);
        eventRepository.delete(event);
    }

    @Override
    public Optional<Event> findById(long id) {
        return eventRepository.findById(id);
    }

    @Override
    public Event getById(long id) {
        Optional<Event> optionalEvent = findById(id);
        if (optionalEvent.isEmpty()) {
            throw new EventNotFoundException(id);
        }
        return optionalEvent.get();
    }

}
