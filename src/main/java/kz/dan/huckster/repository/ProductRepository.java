package kz.dan.huckster.repository;

import kz.dan.huckster.entity.Product;
import kz.dan.huckster.entity.Supplier;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query(value = "from Product where (:supplier is null or supplier =:supplier) and (:name is null or name like lower(concat('%', :name,'%')))",
            countQuery = "select count(id) from Product where (:supplier is null or supplier =:supplier) and (:name is null or lower(name) like lower(concat('%', :name,'%')))")
    PageImpl<Product> findAll(@Param("name") String user,
                              @Param("supplier") Supplier supplier,
                              Pageable pageable);
}
