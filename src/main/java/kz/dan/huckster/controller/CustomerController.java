package kz.dan.huckster.controller;

import kz.dan.huckster.dto.PageDto;
import kz.dan.huckster.dto.customer.CustomerCreateDto;
import kz.dan.huckster.dto.customer.CustomerDto;
import kz.dan.huckster.dto.customer.CustomerListFilterDto;
import kz.dan.huckster.dto.customer.CustomerUpdateDto;
import kz.dan.huckster.service.CustomerService;
import kz.dan.huckster.utils.PageUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/customer")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class CustomerController {
    private final CustomerService customerService;

    @PostMapping
    public ResponseEntity<CustomerDto> create(@Valid @RequestBody CustomerCreateDto customerCreateDto) {
        CustomerDto customerDto = customerService.create(customerCreateDto);
        return new ResponseEntity<>(customerDto, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Page<CustomerDto>> get(@RequestParam(required = false) String name,
                                                PageDto pageDto) {
        CustomerListFilterDto filter = CustomerListFilterDto.builder()
                .name(name)
                .build();
        Page<CustomerDto> all = customerService.get(filter, PageUtils.createPageRequest(pageDto));
        return new ResponseEntity<>(all, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CustomerDto> update(@PathVariable(name = "id") long id, @Valid @RequestBody CustomerUpdateDto customerUpdateDto) {
        CustomerDto customerDto = customerService.update(id, customerUpdateDto);
        return new ResponseEntity<>(customerDto, HttpStatus.OK);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public void delete(@RequestParam(name = "id") long id) {
        customerService.delete(id);
    }
}
