package kz.dan.huckster.dto.size;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SizeListFilterDto {
    private String name;
}
