package kz.dan.huckster.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "product")
@Data
public class Product extends AbstractEntity {
    private String name;
    @ManyToOne
    @JoinColumn(name="supplier_id", nullable=false)
    private Supplier supplier;

    @OneToMany(mappedBy = "product")
    private List<Size> sizes;
}
