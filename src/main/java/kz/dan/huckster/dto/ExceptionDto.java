package kz.dan.huckster.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import kz.dan.huckster.exception.Violation;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ExceptionDto {
    private String message;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private List<Violation> violations;
}
