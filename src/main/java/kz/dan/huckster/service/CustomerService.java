package kz.dan.huckster.service;

import kz.dan.huckster.dto.customer.CustomerCreateDto;
import kz.dan.huckster.dto.customer.CustomerDto;
import kz.dan.huckster.dto.customer.CustomerListFilterDto;
import kz.dan.huckster.dto.customer.CustomerUpdateDto;
import kz.dan.huckster.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface CustomerService {
    CustomerDto create(CustomerCreateDto customerCreateDto);

    Page<CustomerDto> get(CustomerListFilterDto filter, Pageable pageable);

    CustomerDto update(long id, CustomerUpdateDto customerUpdateDto);

    void delete(long id);
    Optional<Customer> findById(long id);
    Customer getById(long id);
}

