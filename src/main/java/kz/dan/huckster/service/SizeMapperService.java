package kz.dan.huckster.service;

import kz.dan.huckster.dto.size.SizeCreateDto;
import kz.dan.huckster.dto.size.SizeDto;
import kz.dan.huckster.entity.Size;

public interface SizeMapperService {
    Size toSize(SizeCreateDto createDto);
    SizeDto toSizeDto(Size size);
}
