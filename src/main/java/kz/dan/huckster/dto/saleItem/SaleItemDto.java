package kz.dan.huckster.dto.saleItem;

import kz.dan.huckster.dto.event.EventDto;
import kz.dan.huckster.dto.product.ProductDto;
import lombok.Data;

@Data
public class SaleItemDto {
    private long id;
    private EventDto event;
    private ProductDto product;
    private double price;
}
