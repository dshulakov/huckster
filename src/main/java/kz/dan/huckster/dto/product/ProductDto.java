package kz.dan.huckster.dto.product;

import kz.dan.huckster.dto.supplier.SupplierDto;
import lombok.Data;

@Data
public class ProductDto {
    private long id;
    private String name;
    private SupplierDto supplier;
}
