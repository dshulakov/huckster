package kz.dan.huckster.dto.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductListFilterDto {
    private String name;
    @JsonProperty("supplier_id")
    private Long supplierId;
}
