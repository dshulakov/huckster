package kz.dan.huckster.dto.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Data
public class ProductUpdateDto {
    @NotBlank
    @Size(min = 1, max = 255)
    private String name;
    @JsonProperty("supplier_id")
    @NotNull
    private Long supplierId;
}
