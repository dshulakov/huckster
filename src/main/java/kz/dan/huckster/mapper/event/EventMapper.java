package kz.dan.huckster.mapper.event;

import kz.dan.huckster.dto.event.EventDto;
import kz.dan.huckster.entity.Event;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

@Component
public class EventMapper extends ModelMapper {
    public EventMapper() {
        super();
        this.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        createTypeMaps();
    }

    public EventDto toEventDto(Event event) {
        return this.map(event, EventDto.class);
    }

    private void createTypeMaps() {
        TypeMap<Event, EventDto> toEventDtoTypeMap = this.createTypeMap(Event.class, EventDto.class);
        toEventDtoTypeMap.addMapping(Event::getId, EventDto::setId);
        toEventDtoTypeMap.addMapping(Event::getName, EventDto::setName);
        toEventDtoTypeMap.addMapping(Event::getCurrency, EventDto::setCurrency);
        toEventDtoTypeMap.addMapping(Event::getRate, EventDto::setRate);
    }

}
