package kz.dan.huckster.service.impl;

import kz.dan.huckster.dto.supplier.SupplierCreateDto;
import kz.dan.huckster.dto.supplier.SupplierDto;
import kz.dan.huckster.dto.supplier.SupplierListFilterDto;
import kz.dan.huckster.dto.supplier.SupplierUpdateDto;
import kz.dan.huckster.entity.Supplier;
import kz.dan.huckster.exception.SupplierNotFoundException;
import kz.dan.huckster.repository.SupplierRepository;
import kz.dan.huckster.service.SupplierMapperService;
import kz.dan.huckster.service.SupplierService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SupplierServiceImpl implements SupplierService {
    private final SupplierRepository supplierRepository;
    private final SupplierMapperService supplierMapperService;


    @Override
    public SupplierDto create(SupplierCreateDto supplierCreateDto) {
        Supplier supplier = supplierMapperService.toSupplier(supplierCreateDto);
        supplierRepository.save(supplier);
        return supplierMapperService.toSupplierDto(supplier);
    }

    @Override
    public Page<SupplierDto> get(SupplierListFilterDto filter, Pageable pageable) {
        Page<Supplier> supplierPage = supplierRepository.findAll(filter.getName(), pageable);
        return supplierPage.map(supplierMapperService::toSupplierDto);
    }

    @Override
    public SupplierDto update(long id, SupplierUpdateDto supplierUpdateDto) {
        Optional<Supplier> optionalSupplier = supplierRepository.findById(id);
        if (optionalSupplier.isEmpty()) {
            throw new RuntimeException("supplier not found");
        }
        Supplier supplier = optionalSupplier.get();
        supplier.setName(supplierUpdateDto.getName());
        supplierRepository.save(supplier);
        return supplierMapperService.toSupplierDto(supplier);
    }

    @Override
    public void delete(long id) {
        Optional<Supplier> optionalProduct = supplierRepository.findById(id);
        if (optionalProduct.isEmpty()) {
            throw new RuntimeException("supplier not found");
        }
        Supplier supplier = optionalProduct.get();
        supplierRepository.delete(supplier);
    }

    @Override
    public Optional<Supplier> findById(long id) {
        return supplierRepository.findById(id);
    }

    @Override
    public Supplier getById(Long id) {
        Optional<Supplier> optionalEvent = findById(id);
        if (optionalEvent.isEmpty()) {
            throw new SupplierNotFoundException(id);
        }
        return optionalEvent.get();
    }

    @Override
    public List<SupplierDto> getAll() {
        List<Supplier> suppliers = supplierRepository.findAll();
        return supplierMapperService.toListSupplierDto(suppliers);
    }

}
