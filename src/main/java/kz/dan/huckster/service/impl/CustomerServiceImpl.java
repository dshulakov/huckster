package kz.dan.huckster.service.impl;

import kz.dan.huckster.dto.customer.CustomerCreateDto;
import kz.dan.huckster.dto.customer.CustomerDto;
import kz.dan.huckster.dto.customer.CustomerListFilterDto;
import kz.dan.huckster.dto.customer.CustomerUpdateDto;
import kz.dan.huckster.entity.Customer;
import kz.dan.huckster.exception.CustomerNotFoundException;
import kz.dan.huckster.mapper.customer.CustomerMapper;
import kz.dan.huckster.repository.CustomerRepository;
import kz.dan.huckster.service.CustomerMapperService;
import kz.dan.huckster.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;
    private final CustomerMapperService customerMapperService;

    @Override
    public CustomerDto create(CustomerCreateDto customerCreateDto) {
        Customer customer = customerMapperService.toCustomer(customerCreateDto);
        customerRepository.save(customer);
        return customerMapperService.toCustomerDto(customer);
    }

    @Override
    public Page<CustomerDto> get(CustomerListFilterDto filter, Pageable pageable) {
        Page<Customer> customerPage = customerRepository.findAll(filter.getName(), pageable);
        return customerPage.map(customerMapperService::toCustomerDto);
    }

    @Override
    public CustomerDto update(long id, CustomerUpdateDto customerUpdateDto) {
        Customer customer = getById(id);
        customer.setName(customerUpdateDto.getName());
        customerRepository.save(customer);
        return customerMapperService.toCustomerDto(customer);
    }

    @Override
    public void delete(long id) {
        Customer customer = getById(id);
        customerRepository.delete(customer);
    }

    @Override
    public Optional<Customer> findById(long id) {
        return customerRepository.findById(id);
    }

    @Override
    public Customer getById(long id) {
        Optional<Customer> optional = findById(id);
        if (optional.isEmpty()) {
            throw new CustomerNotFoundException(id);
        }
        return optional.get();
    }

}
