package kz.dan.huckster.service;

import kz.dan.huckster.dto.event.EventCreateDto;
import kz.dan.huckster.dto.event.EventDto;
import kz.dan.huckster.entity.Event;

public interface EventMapperService {
    Event toEvent(EventCreateDto createDto);
    EventDto toEventDto(Event event);
}
