package kz.dan.huckster.mapper.supplier.converter;

import kz.dan.huckster.entity.Supplier;
import kz.dan.huckster.service.SupplierService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SupplierIdToEntityConverter implements Converter<Long, kz.dan.huckster.entity.Supplier> {
    private final SupplierService supplierService;

    @Override
    public Supplier convert(MappingContext<Long, Supplier> mappingContext) {
        return supplierService.getById(mappingContext.getSource());
    }
}
