package kz.dan.huckster.service;

import kz.dan.huckster.dto.supplier.SupplierCreateDto;
import kz.dan.huckster.dto.supplier.SupplierDto;
import kz.dan.huckster.dto.supplier.SupplierListFilterDto;
import kz.dan.huckster.dto.supplier.SupplierUpdateDto;
import kz.dan.huckster.entity.Event;
import kz.dan.huckster.entity.Supplier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface SupplierService {
    SupplierDto create(SupplierCreateDto supplierCreateDto);

    Page<SupplierDto> get(SupplierListFilterDto filter, Pageable pageable);

    SupplierDto update(long id, SupplierUpdateDto supplierUpdateDto);

    void delete(long id);

    Optional<Supplier> findById(long id);

    Supplier getById(Long id) ;

    List<SupplierDto> getAll();
}

