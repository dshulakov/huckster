package kz.dan.huckster.controller;

import kz.dan.huckster.dto.PageDto;
import kz.dan.huckster.dto.supplier.SupplierCreateDto;
import kz.dan.huckster.dto.supplier.SupplierDto;
import kz.dan.huckster.dto.supplier.SupplierListFilterDto;
import kz.dan.huckster.dto.supplier.SupplierUpdateDto;
import kz.dan.huckster.service.SupplierService;
import kz.dan.huckster.utils.PageUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/supplier")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class SupplierController {
    private final SupplierService supplierService;

    @PostMapping
    public ResponseEntity<SupplierDto> create(@Valid @RequestBody SupplierCreateDto supplierCreateDto) {
        SupplierDto supplierDto = supplierService.create(supplierCreateDto);
        return new ResponseEntity<>(supplierDto, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Page<SupplierDto>> get(@RequestParam(required = false) String name,
                                                PageDto pageDto) {
        SupplierListFilterDto filter = SupplierListFilterDto.builder()
                .name(name)
                .build();
        Page<SupplierDto> all = supplierService.get(filter, PageUtils.createPageRequest(pageDto));
        return new ResponseEntity<>(all, HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<List<SupplierDto>> getAll() {
        List<SupplierDto> all = supplierService.getAll();
        return new ResponseEntity<>(all, HttpStatus.OK);
    }


    @PutMapping("/{id}")
    public ResponseEntity<SupplierDto> update(@PathVariable(name = "id") long id, @Valid @RequestBody SupplierUpdateDto supplierUpdateDto) {
        SupplierDto supplierDto = supplierService.update(id, supplierUpdateDto);
        return new ResponseEntity<>(supplierDto, HttpStatus.OK);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public void delete(@RequestParam(name = "id") long id) {
        supplierService.delete(id);
    }
}
