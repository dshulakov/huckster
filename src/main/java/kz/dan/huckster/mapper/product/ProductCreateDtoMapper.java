package kz.dan.huckster.mapper.product;

import kz.dan.huckster.dto.product.ProductCreateDto;
import kz.dan.huckster.entity.Product;
import kz.dan.huckster.mapper.supplier.converter.SupplierIdToEntityConverter;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

@Component
public class ProductCreateDtoMapper extends ModelMapper {
    public ProductCreateDtoMapper(SupplierIdToEntityConverter supplierIdToEntityConverter) {
        super();
        this.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        this.addConverter(supplierIdToEntityConverter);
        createTypeMaps();
    }

    public Product toProduct(ProductCreateDto productCreateDto) {
        return this.map(productCreateDto, Product.class);
    }

    private void createTypeMaps() {
        TypeMap<ProductCreateDto, Product> toProductTypeMap = this.createTypeMap(ProductCreateDto.class, Product.class);
        toProductTypeMap.addMapping(ProductCreateDto::getName, Product::setName);
        toProductTypeMap.addMapping(ProductCreateDto::getSupplierId, Product::setSupplier);
    }
}
