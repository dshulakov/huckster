package kz.dan.huckster.repository;

import kz.dan.huckster.entity.Supplier;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SupplierRepository extends JpaRepository<Supplier, Long> {
    @Query(value = "from Supplier where (:name is null or name like lower(concat('%', :name,'%')))",
            countQuery = "select count(id) from Supplier where (:name is null or lower(name) like lower(concat('%', :name,'%')))")
    PageImpl<Supplier> findAll(@Param("name") String user, Pageable pageable);
}
